require 'benchmark'

class User < ApplicationRecord

  def self.benchmark
    Benchmark.bmbm do
      100.times do
        threads = []
        p "=================================="
        p "Send 1000 requests at a time"

        current_time = Time.now
        1000.times do
          threads << Thread.new { RestClient.get('http://localhost:3000/users') }
        end

        threads.each(&:join)
        seconds_taken = Time.now - current_time
        p "took #{seconds_taken} seconds"
        p "throughput #{seconds_taken / 1000} rps"
        sleep 1
      end
    end
  end
end

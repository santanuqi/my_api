santanu@IN-LT-53 ~/dev/my_api (ruby-version)$ab -n 2000 -c 200 http://localhost:3000/users
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)
Completed 200 requests
Completed 400 requests
Completed 600 requests
Completed 800 requests
Completed 1000 requests
Completed 1200 requests
Completed 1400 requests
Completed 1600 requests
Completed 1800 requests
Completed 2000 requests
Finished 2000 requests


Server Software:
Server Hostname:        localhost
Server Port:            3000

Document Path:          /users
Document Length:        18073 bytes

Concurrency Level:      200
Time taken for tests:   30.977 seconds
Complete requests:      2000
Failed requests:        0
Total transferred:      37044000 bytes
HTML transferred:       36146000 bytes
Requests per second:    64.56 [#/sec] (mean)
Time per request:       3097.744 [ms] (mean)
Time per request:       15.489 [ms] (mean, across all concurrent requests)
Transfer rate:          1167.81 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  622 1510.8      0   15419
Processing:    29 2192 392.4   2192    3285
Waiting:       29 2191 392.4   2192    3284
Total:         35 2814 1599.6   2236   18019

Percentage of the requests served within a certain time (ms)
  50%   2236
  66%   2967
  75%   3168
  80%   3212
  90%   3619
  95%   5340
  98%   9345
  99%   9475
 100%  18019 (longest request)
